-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: laravel_crud
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB-1:10.3.17+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catagories`
--

DROP TABLE IF EXISTS `catagories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catagories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catagories`
--

LOCK TABLES `catagories` WRITE;
/*!40000 ALTER TABLE `catagories` DISABLE KEYS */;
INSERT INTO `catagories` VALUES (1,'Darian Kessler','Ut earum voluptas sapiente sed. Sit facere provident aut magni eos reprehenderit numquam. Blanditiis molestiae repellendus soluta cupiditate nemo et. Enim provident perspiciatis et est quod eum sit. Dolor distinctio ut quis debitis laudantium eligendi. To','217','2019-08-27 12:17:07','2019-08-09 10:29:11'),(2,'Alexandro Haag','Qui at placeat cupiditate omnis culpa. Non cupiditate esse nesciunt ab eos tenetur et consequatur. Ea ut doloremque ea qui repellendus reiciendis enim. Ipsum voluptatem enim asperiores necessitatibus commodi qui non. Et non ut est dolore quia qui. Saepe a','1586978','2019-08-27 12:17:07','2019-08-04 12:23:51'),(3,'Nestor Kris MD','Enim sunt voluptate fuga vel eius. Minima aspernatur atque autem odio fugit dolores nostrum et. Id officiis molestiae nam saepe ab cum vel voluptatem. At consequatur et et iure adipisci. Est est nulla autem et deserunt ut. Deserunt sit quo quaerat nihil n','65521','2019-08-27 12:17:08','2019-08-01 07:49:47'),(4,'Ernestina Waters','Voluptas nam fuga inventore perspiciatis tempora delectus modi. Quo repudiandae modi qui qui. Aut in ut facere temporibus et error officia. Animi deleniti est quos. Officia ullam saepe harum dolorum omnis distinctio. Quia tempore iste ut consectetur. Even','72281','2019-08-27 12:17:08','2019-08-03 15:03:56'),(5,'Ms. Bailee Bernier','Non et laboriosam explicabo necessitatibus. Modi voluptates molestiae delectus quis earum explicabo. Non velit sit voluptates provident placeat qui non. Explicabo sint dolorum voluptatem sunt. Ut qui ex rerum sed. Et quam cum maxime iure aliquam corporis.','2','2019-08-27 12:17:08','2019-08-19 17:14:59'),(6,'Darien Klein','Iusto reiciendis sunt in ullam eaque odit. Voluptatem suscipit quam voluptate explicabo culpa saepe ea. Sed cumque ut sed minima consequatur vel. Laborum placeat qui quia exercitationem laborum at. Et expedita voluptatem id autem aut magnam autem. Officia','52988907','2019-08-27 12:17:08','2019-08-05 09:05:33'),(7,'Ray Herman DVM','Et placeat dolor consequuntur non. Odio suscipit architecto praesentium quia saepe impedit. Accusantium distinctio nam et sit occaecati repudiandae blanditiis. Cumque et ex consequatur maxime. Atque dolore ullam et voluptas nemo. Et non quae ratione est c','6591','2019-08-27 12:17:08','2019-08-18 22:24:01'),(8,'Krista Turner','Excepturi illo officiis ut hic laboriosam quam assumenda ipsam. Delectus deleniti soluta aut aperiam illum quaerat. Dolores voluptatibus ratione laudantium ea voluptatum consequatur hic aliquam. Nobis at odio omnis asperiores. Consequatur non excepturi co','60','2019-08-27 12:17:08','2019-08-19 13:12:26'),(9,'Dr. Amira Wolf II','Quia possimus labore optio earum blanditiis a omnis. Sit dolorum sed voluptate. Voluptas voluptatem minus quibusdam illum. Dolore ut rerum et asperiores alias. A dolore impedit hic est. Neque et autem id voluptatibus ipsam reiciendis. Consectetur molestia','9703582','2019-08-27 12:17:08','2019-08-25 07:33:04'),(10,'Mrs. Robyn Rogahn','Qui neque consequatur aut eius maiores magnam. Tempora harum illum deleniti optio voluptatem. Corporis accusantium odit tenetur quo id facilis. Sit debitis quia deleniti corporis. Ratione optio provident quisquam possimus numquam. Quas asperiores minus es','9','2019-08-27 12:17:08','2019-08-22 19:00:42'),(11,'Haven Kuhic','Dignissimos vel pariatur sit voluptas fuga et. Rerum eum ipsam doloribus atque sapiente. Consequuntur id ullam vero quidem reprehenderit. Consequatur iste facilis atque ex mollitia qui in officiis. Commodi voluptas aut voluptates. Velit qui voluptatem mol','28','2019-08-27 12:17:08','2019-08-24 12:13:47'),(12,'Beulah Denesik III','Doloribus ex eos fugit similique esse. Qui ab amet molestias sapiente voluptatem incidunt. Nostrum et id magnam ea. Exercitationem animi et placeat consequatur dolores quibusdam voluptas. Quam quidem ab vel et. Neque nisi mollitia suscipit asperiores. Sed','51376144','2019-08-27 12:17:08','2019-08-01 14:37:18'),(13,'Dr. Monica Dare','Ex eveniet laudantium unde facilis ut in architecto. Beatae labore hic eaque tenetur debitis doloremque. Rerum laborum dolor ab tempore. Dolorem provident quia eum natus. Voluptatibus consequatur nihil tempora quisquam officia. Deserunt omnis ducimus quia','327409','2019-08-27 12:17:08','2019-08-15 05:06:36'),(14,'Lonzo Hartmann MD','Ut consequatur ipsa saepe porro adipisci qui. Voluptas recusandae necessitatibus alias quasi dolores earum delectus. Omnis et mollitia atque voluptates ad incidunt nesciunt. Et et provident delectus aut. Impedit non doloremque consequuntur maxime. Id ut u','610','2019-08-27 12:17:08','2019-08-25 05:10:57'),(15,'Josianne Denesik','Ut voluptas asperiores omnis eius. Eveniet excepturi inventore cum ea explicabo neque. Consequatur voluptates a ipsum molestias quia itaque. Debitis mollitia non qui. Et repellendus laborum laborum quo sapiente. Architecto voluptatibus ab et sed eos vero.','86','2019-08-27 12:17:08','2019-08-27 11:14:12'),(16,'Gerry Simonis MD','Ut quibusdam dolorem sapiente eaque soluta quia illum. Tempora eos voluptas minus impedit. Explicabo ut numquam id dolorem et totam similique. Rem aperiam animi sint porro incidunt deserunt. Repellat eveniet molestias dignissimos ipsum nam eum. Impedit it','962997','2019-08-27 12:17:08','2019-08-19 04:10:19'),(17,'Arturo Bartoletti','Non tempora quidem quis autem voluptate ut iure maxime. Corporis ut et dolore voluptatibus voluptatibus quis itaque inventore. Quos quia consequatur eius odit sit tenetur et expedita. Culpa est quos assumenda illo voluptas ad necessitatibus. Soluta possim','35788316','2019-08-27 12:17:09','2019-08-24 22:38:15'),(18,'Felton Thompson','Nostrum officiis repellat exercitationem et explicabo sit et. Fugiat hic numquam qui laboriosam qui soluta. Nostrum reiciendis et aut ratione expedita. Qui voluptatem illum aut eum veniam. Quo veritatis temporibus qui harum vitae dolor aut. Sit eligendi d','2165582','2019-08-27 12:17:09','2019-08-09 04:42:10'),(19,'Mrs. Charlene Windler III','Libero dolores atque unde tenetur repellendus molestiae sed molestiae. Neque nesciunt cumque blanditiis. Dolores et rem iure non magnam voluptas. Reiciendis voluptatem voluptatem nostrum necessitatibus alias. Voluptates officia nihil qui dignissimos. Quae','705159','2019-08-27 12:17:09','2019-08-21 09:13:23'),(20,'Garfield Koch','Et asperiores explicabo dolores suscipit facilis molestiae. Maiores alias accusantium occaecati nemo est. Ab ut vel est atque sunt sint. Quasi est aut laboriosam qui. Ipsa beatae quaerat eligendi sapiente reiciendis corporis quia. Incidunt fuga occaecati ','966159','2019-08-27 12:17:09','2019-08-16 11:41:17'),(21,'Mr. Tod Schumm DVM','Delectus aliquid at et. Id dolores velit maxime quos explicabo sit autem. Rerum ad qui omnis molestiae eum mollitia tenetur. Cupiditate earum voluptas aut inventore vero. Eos vel velit sit. Blanditiis molestiae nam cum libero voluptatibus eveniet. Ea volu','2764','2019-08-27 12:17:09','2019-08-24 04:43:46'),(22,'Mrs. Ruthe McLaughlin III','Et quia et dolor et necessitatibus cupiditate. Vel similique quasi qui voluptas rerum ut mollitia. Nihil accusantium recusandae beatae voluptatum necessitatibus et nam. Mollitia fuga temporibus optio et cupiditate magni quis optio. Et voluptates nulla vol','5','2019-08-27 12:17:09','2019-08-08 05:58:38'),(23,'Mrs. Margie Sporer','Qui impedit repellendus non at voluptatibus. Consequatur quidem est itaque reprehenderit. Eos repellendus rem voluptates inventore beatae. Nisi in rerum dolore quis. Quidem deserunt et earum blanditiis pariatur optio. Rem velit recusandae ut doloribus quo','89387149','2019-08-27 12:17:09','2019-08-09 07:17:07'),(24,'Brooklyn Lockman','Iusto et deleniti laborum libero. Neque fugit ut quas ex. Occaecati tempore non in. Et nam optio iste ut veritatis. Minus nesciunt quisquam totam autem. Ducimus sed suscipit dolorem autem omnis corporis. Eligendi laboriosam at aut labore. Ut et quis ut su','38776479','2019-08-27 12:17:09','2019-07-31 08:27:37'),(25,'Alfonso Spencer','Commodi aut eaque dolorem delectus et impedit rerum. Dolor eaque voluptas ad vero architecto praesentium corrupti illum. Nesciunt ratione reprehenderit consectetur reprehenderit ipsam eos. Vero iste reiciendis veritatis et. Omnis corporis reprehenderit et','2935863','2019-08-27 12:17:09','2019-07-28 05:57:23'),(26,'Dr. Jaren Bernier','Ut ut sed voluptates voluptatem quisquam iste. Vel et nobis aspernatur neque occaecati aut quam reiciendis. Ratione quod harum reprehenderit porro. Quia quidem cum ipsum et. Consequatur non et commodi non molestiae qui. Sunt consequatur et alias dolor mai','163373159','2019-08-27 12:17:09','2019-08-08 19:09:34'),(27,'Prof. Micaela Purdy','Veniam doloremque dolore quia ipsa consectetur. Harum distinctio vero earum quia quasi unde ut. In eum enim in qui consequatur accusantium corrupti. Voluptate perferendis ipsum et fuga. Sit molestiae velit praesentium eum sit. Dolores eligendi quisquam qu','7425','2019-08-27 12:17:09','2019-08-20 05:47:32'),(28,'David Stiedemann V','Ipsam magnam assumenda dolore quis sit minima quia. Deserunt officia quia sapiente voluptatem. Quae magni quod fuga doloremque ut. Numquam fuga at beatae eos. Enim id porro dolor sit repellat. Perspiciatis dignissimos molestias dolores accusantium. Enim v','5','2019-08-27 12:17:09','2019-08-05 04:31:43'),(29,'Ned Balistreri','Dolorum illo omnis voluptate iste architecto repellendus sequi. Vitae nihil laborum laboriosam autem nemo dolore rem. Cumque quis quasi facere qui dolore nam natus. A id rerum quis. Nobis mollitia sapiente quos eius beatae dolorem. Repellat sunt saepe iur','664003','2019-08-27 12:17:09','2019-08-14 12:31:44'),(30,'Edwin Dickinson','Incidunt ipsam doloremque enim dolor repellat. Aspernatur at aliquam illo sequi aut. Eveniet qui exercitationem cum numquam quia et quisquam. Aspernatur veniam fuga aut vel fuga hic. Minima laudantium dolorem sequi quia error occaecati. Et libero repellat','9882','2019-08-27 12:17:09','2019-08-11 08:28:33');
/*!40000 ALTER TABLE `catagories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2019_08_12_000000_create_users_table',1),(2,'2019_08_12_100000_create_password_resets_table',1),(3,'2019_08_26_140523_create_products_table',1),(4,'2019_08_26_142353_create_catagories_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `catagory_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,0,1,'Boll Automatikfilter 6.18','produkt-1.jpg','Nostrum alias ad earum voluptatibus. Aperiam nulla rerum est voluptas ullam cupiditate. Perferendis velit repellendus veniam suscipit. Illum iure qui et hic sed. Harum ullam perferendis id repellendus nemo sequi voluptas. Velit ullam velit in eum reici','10 000','2019-08-27 12:17:06','2019-08-27 14:07:59'),(2,0,2,'Boll Automatikfilter 6.18','produkt-2.jpg','Ab tenetur perferendis voluptas eum optio dolor. Molestias voluptas facere velit voluptatibus. Enim voluptas assumenda voluptatum et sit dolorum. Ipsam velit rerum ut et. Ut et aut magnam aut consequatur iure. Recusandae deleniti commodi voluptas qui reru','E27 000','2019-08-27 12:17:06','2019-08-27 13:17:29'),(3,0,1,'Boll Automatikfilter 6.18','produkt-3.jpg','Repudiandae laudantium adipisci placeat omnis quisquam quis. Distinctio assumenda dolores ipsam illum velit. Laborum distinctio deserunt magnam est vitae. Ut ea quo natus optio quo id reiciendis quidem. Nihil consequatur nihil rem aut quae voluptatem. Vel','97','2019-08-27 12:17:06','2019-08-17 23:17:58'),(4,0,0,'Boll Automatikfilter 6.18','produkt-1.jpg','Amet illum perferendis repellat quo aspernatur odit. Possimus rem cum sit commodi minus est sit. Aut est et est quis est autem. Est inventore temporibus sed natus eaque consequatur. Deleniti qui ipsam doloremque nostrum. Magni est distinctio consequatur e','45','2019-08-27 12:17:06','2019-08-04 11:15:02'),(5,0,0,'Boll Automatikfilter 6.18','produkt-2.jpg','Voluptatem voluptatem ea assumenda. Fugiat voluptatem vitae possimus blanditiis suscipit est neque. Iusto quisquam at tempore quae sed eligendi. Eius ex et officia nam voluptatem et non. Accusamus soluta labore excepturi laboriosam ut neque ipsam impedit.','48','2019-08-27 12:17:06','2019-08-16 05:52:44'),(6,0,0,'Boll Automatikfilter 6.18','produkt-3.jpg','Et consequatur consectetur exercitationem ea pariatur sed enim. Neque voluptatum fuga quas quia et. Neque vel consequatur sed excepturi nulla voluptas tempore. Eos tempore perspiciatis facilis illo et earum aut. Quo temporibus officia in eligendi laborum ','64565','2019-08-27 12:17:06','2019-08-27 11:51:49'),(7,0,0,'Joany Parisian',NULL,'Temporibus expedita placeat facere unde. Autem velit nobis non inventore aut ad. Dolor architecto esse sit et asperiores numquam veritatis. Fugit nesciunt nesciunt voluptas nihil est libero. Dolorem eos adipisci culpa voluptatibus. Consequuntur dolorum bl','964','2019-08-27 12:17:06','2019-07-30 10:26:28'),(8,0,0,'Vallie Rice I',NULL,'Ut voluptas est modi et. Fugit enim aperiam ea libero quis itaque. Repudiandae est expedita ut quia minima in minima impedit. Consequatur voluptas dolor asperiores. Doloribus magnam vel voluptas quam nostrum ratione. Iure unde odit rem ipsa deleniti. Dign','3','2019-08-27 12:17:06','2019-08-24 13:51:47'),(9,0,0,'Hildegard Goodwin',NULL,'Et ullam ut dolorem sed occaecati. Rerum sint quia pariatur unde accusamus iste vero. Qui veniam eos sit qui. Odio distinctio aspernatur qui est ipsum. Velit doloribus repudiandae dolor et. Molestias facilis non voluptas at deserunt nisi possimus. Est ver','24','2019-08-27 12:17:06','2019-08-24 00:46:30'),(10,0,0,'Annabelle Stark',NULL,'Et fuga non quo iure. Similique eaque sunt sed rerum quam aut. Voluptatem repellendus sed consequatur placeat dolorem nobis et vel. Ea ullam eligendi sunt iusto. Corporis deserunt aperiam necessitatibus modi. Et sit id quo. Velit aperiam dolores nisi iure','685544','2019-08-27 12:17:06','2019-08-04 23:02:09'),(11,0,0,'Kaela Treutel',NULL,'Ut et doloribus harum. Sit aut natus quae id cum. Autem aut ut neque ut. Et fugit necessitatibus excepturi architecto. Facilis omnis repellendus voluptatem enim consequatur voluptatem. Nihil laborum maiores optio odio possimus molestiae. Et laborum volupt','287164','2019-08-27 12:17:06','2019-08-10 04:30:25'),(12,0,0,'Jackson Ortiz',NULL,'Consequatur sequi voluptatem voluptatem optio sequi. Natus iusto et aut vel. Voluptate cum molestiae similique dignissimos accusantium sequi. Ab quis officia possimus et. Veniam quaerat nesciunt voluptatibus totam. Iste fugiat et rerum debitis ut sint. Po','3','2019-08-27 12:17:06','2019-08-20 13:55:46'),(13,0,0,'Johnson Altenwerth II',NULL,'Autem quos nam nostrum eveniet. Non sunt qui voluptate dolorum vitae. Quos consequatur enim sit dolor. Dolor ea excepturi pariatur inventore. Aut sunt est ut id. Qui est ratione fugiat culpa laboriosam cum. Quia a ullam aliquid beatae iure aspernatur volu','785686','2019-08-27 12:17:06','2019-08-08 10:58:27'),(14,0,0,'Olga Daniel',NULL,'Eos praesentium ratione in blanditiis. Voluptatem itaque itaque dolores natus vitae optio inventore. Ea suscipit maiores sit sint. Omnis sunt error et. Id voluptatibus optio delectus cupiditate dolorem quia libero odit. Dolore aliquid non officia nemo. Ab','64','2019-08-27 12:17:06','2019-08-18 02:26:46'),(15,0,0,'Rudy Schinner',NULL,'Natus maiores mollitia vero voluptatem quia. Fuga eum beatae veritatis veniam fugit. Voluptatem est velit ut qui non. Corrupti at quae blanditiis iste assumenda. Id modi illo nihil quos aut quibusdam illo. Asperiores harum unde perferendis aut. Voluptatem','347253366','2019-08-27 12:17:07','2019-08-18 22:33:22'),(16,0,0,'Madeline Bailey',NULL,'Quas in beatae quis alias ut quis dolorem. Quidem qui magnam numquam voluptatem placeat. Illum totam ipsa necessitatibus eaque quasi beatae iure ea. Recusandae autem occaecati magni. Eaque vero dignissimos ipsam impedit magni. Dignissimos aut corporis lib','216555','2019-08-27 12:17:07','2019-07-27 21:19:05'),(17,0,0,'Braulio Halvorson IV',NULL,'Vel sit a fugiat ut provident veritatis. Laboriosam ut totam error voluptatem. Impedit enim ut modi. Illum velit dolorum repellat earum nulla. Cum ad unde impedit ipsa excepturi. Dicta dolor rerum quibusdam id enim ex. Nam modi ducimus illum illum. Et imp','49332','2019-08-27 12:17:07','2019-08-21 15:30:52'),(18,0,0,'Aubree Harvey',NULL,'Quia sint ducimus quia et commodi ut. Soluta neque et et autem ut. Atque sequi ipsam est omnis voluptas cupiditate. Dicta consequatur dolorem aut doloremque voluptatem nobis iure. Sunt voluptates reiciendis necessitatibus unde rerum praesentium perspiciat','357','2019-08-27 12:17:07','2019-08-27 02:29:04'),(19,0,0,'Mr. Kayden Cartwright',NULL,'Aliquid ullam officia vero ipsam magnam tenetur velit. Sed ut eligendi in dolorem ut quas et. Ad voluptatem omnis deserunt consequatur laudantium. Blanditiis est at consequuntur molestias. Pariatur fuga dolor doloribus et. Excepturi minus ut est aliquam e','5','2019-08-27 12:17:07','2019-08-03 01:07:42'),(20,0,0,'Zoe O\'Connell',NULL,'At accusamus quae veritatis. Porro quas necessitatibus fugit inventore aliquid. Temporibus voluptatem aliquid dolores voluptas et quia et. Est veritatis ad qui molestiae quo harum quia. Autem voluptate occaecati sed delectus quasi qui aut. Quia eum perspi','11043763','2019-08-27 12:17:07','2019-08-25 07:48:19'),(21,0,0,'Dr. King Upton',NULL,'Doloremque quis saepe consequatur eos. Sed est nam sed molestiae aliquid dignissimos sed non. Neque delectus vel voluptatibus repellat delectus earum. Ipsam iusto tempora maxime quisquam quod consectetur. Earum magni dolor laboriosam autem possimus necess','205740208','2019-08-27 12:17:07','2019-08-04 19:05:56'),(22,0,0,'Prof. Kacey Rutherford PhD',NULL,'Voluptatibus animi ea adipisci nostrum. Assumenda cupiditate perferendis ut optio atque adipisci. Facilis corporis quisquam deleniti commodi explicabo est assumenda. Possimus eaque voluptas eos aut quae maiores. Et consequatur nihil impedit. Expedita dolo','70634118','2019-08-27 12:17:07','2019-07-31 19:33:50'),(23,0,0,'Lisa Gislason',NULL,'Maiores sunt quia minus omnis corrupti. Est et ut velit commodi nostrum. Cumque optio deleniti et labore debitis. Neque ducimus nulla molestiae et. Eum ducimus ad iste quos sunt. Sit ut animi et nihil. Voluptatem molestiae nostrum et dicta minus atque. Qu','2308','2019-08-27 12:17:07','2019-08-09 06:29:31'),(24,0,0,'Prof. Linwood Balistreri I',NULL,'Fugit alias ut esse aut vel. Adipisci repudiandae nulla soluta nulla. Ut optio saepe earum provident. Vitae nihil cupiditate aspernatur nulla cupiditate. Reiciendis sit officiis similique ipsum ea. Debitis harum ea asperiores voluptatem perspiciatis et fa','82655433','2019-08-27 12:17:07','2019-08-09 06:01:08'),(25,0,0,'Ariane Brown',NULL,'Expedita numquam unde perferendis vel sint doloribus minus. Dolor sed illum pariatur voluptas quibusdam sed et et. Vero eaque architecto voluptates dolore expedita est sed enim. Tempora totam laboriosam consequatur non. Dolor a voluptatem quod sed. Enim a','238928','2019-08-27 12:17:07','2019-07-29 13:54:09'),(26,0,0,'Wilbert Hettinger MD',NULL,'Excepturi veritatis numquam id nemo similique mollitia. Laborum vel qui possimus. Necessitatibus dolores eos et tempore ut qui quidem. Molestiae autem aliquam veritatis harum ex. Voluptates dolorum voluptas doloremque animi reprehenderit non. Corrupti con','25','2019-08-27 12:17:07','2019-08-10 11:46:07'),(27,0,0,'Pattie Cremin',NULL,'Perspiciatis sunt quaerat qui corrupti quis ipsum. Ut inventore aperiam non ipsa sunt nobis. Porro iusto et ut. Aliquam minima veniam delectus nostrum earum. Illo dolore voluptatum quia qui et asperiores mollitia. Ut reprehenderit quod atque enim molestia','3360636','2019-08-27 12:17:07','2019-08-06 05:07:12'),(28,0,0,'Prof. Eudora Dickinson DDS',NULL,'Veritatis quas quia officiis in deleniti. Vero aut quas odit autem. Doloremque blanditiis repudiandae consectetur magnam. Laborum fugit voluptas qui natus. Fugit quae inventore velit sapiente. Enim voluptas dicta quibusdam qui. Quae sit molestias facere e','10108675','2019-08-27 12:17:07','2019-08-16 02:32:09'),(29,0,0,'Ms. Delphine Torphy',NULL,'Quasi corporis quasi illum ipsam quas impedit ut. Qui sit temporibus numquam suscipit accusamus. Est cumque est voluptas dignissimos ut. Culpa quo molestiae provident reprehenderit corrupti. Pariatur rerum minima et nobis repellendus dolor nulla ullam. Sa','448346','2019-08-27 12:17:07','2019-07-30 03:43:48'),(30,0,0,'Miss Leonie Denesik',NULL,'Perspiciatis et velit voluptate incidunt. Dolorem velit reprehenderit quae deleniti. Sunt repellendus impedit doloribus. Totam at nam est odit. Cum non ad similique dolore id et delectus. Nihil aut omnis eos ipsa illum qui ut. Inventore nesciunt et assume','833056960','2019-08-27 12:17:07','2019-08-23 02:58:27'),(31,0,0,'Produkt 2','/tmp/php3VedoZ','<p>sdfgdfs dsfg sdfg sdfgsd</p>','4567','2019-08-27 13:04:04','2019-08-27 13:04:04');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Des','des@live.co.za',NULL,'$2y$10$PY8I1rJy/W.woHOyrl/LLe241O7J3Pj0Iv1JOxKisZfI.6VLKeMb2','9gH8wz6bxifyWMsnGt7paTJiMusGMeoeziN0i87DLOOUAXx34NtmkMJqgV87','2019-08-27 12:17:05','2019-08-27 12:17:05');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-27 19:36:46
