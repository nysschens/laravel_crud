<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed
        //App\Product::create([
        DB::table('products')->insert([
            'name' => Str::random(10),
            'description' => Str::random(10).' some description',
            'price' => Str::random(10),
        ]);
    }
}
