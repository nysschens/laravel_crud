<?php

use Illuminate\Database\Seeder;

class CatagoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed
        App\Catagory::create([
            //DB::table('catagories')->insert([
                'name' => Str::random(10),
                'description' => Str::random(10).' some description',
                'code' => Str::random(10),
            ]);
        
    }
}
