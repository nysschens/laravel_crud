<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        App\User::create([
            'name' => 'Des',
            'email' => 'des@live.co.za',
            'password' => bcrypt('des'),
        ]);

        /*
        $this->call([
            ProductsTableSeeder::class,
            CatagoriesTableSeeder::class,
        ]);
        */

        factory(App\Product::class, 30)->create();
        factory(App\Catagory::class, 30)->create();
    }
}
