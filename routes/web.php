<?php

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('product/add', 'ProductController@create');
Route::get('product/{product}/delete', [
    'as'   => 'product.delete',
    'uses' => 'ProductController@destroy',
]);
Route::resource('/product', 'ProductController');

/**
 * @param $key
 * @param string $direction
 * @return bool
 */
if (!function_exists('is_active_sorter')) {
    function is_active_sorter($key, $direction = 'ASC')
    {
        if (request('sortby') == $key && request('sortdir') == $direction) {
            return true;
        }

        return false;
    }
}

