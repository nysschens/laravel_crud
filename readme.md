# For GSDH 
A simple Laravel CRUD app

## Installation

There 2 two ways we can install the app.
However, you will need composer, just in case.

Installation 1 <small>quick way to get things working</small>
```
- Untar the archive in your DocumentRoot or any dev project direct.
- Edit the .env file and update with your MySQL or MariaDB username and password details.
- Create an empty new database with name laravel_crud
- Run command from your mysql-prompt> source laravel_crud_dump.sql; 
```
The above simply creates an instance of the current data in 
/database/backup/laravel_crud_dump.sql


Installation 2 [ a fresh laravel instance with dummy data seeding]

```
- Edit the .env file and update with your MySQL or MariaDB username and password details.
- Create the database laravel_crud 
- php artisan migrate --seed
```

To run 
```
php artisan serve
```

Visit http://localhost:8000 in your browser. 
The default front page / home follows the design.

## Notes
The link 'PRODUKTE' is a secure link and will present the login form.

Login with the details:
- username:  des@live.co.za
- password:  des  [or demo]


This GSDH app uses MySQL database by default. 
If you want to use another database, edit configuration in `config/database.php`.

-----------
Enjoy!

Des
