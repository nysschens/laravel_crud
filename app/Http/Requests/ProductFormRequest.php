<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $productId = '';

        if (isset($this->product->id)) {
            $productId = $this->product->id;
        }

        return [
            //'url' => 'required|unique:products,url,'.$productId,
            'user_id' => 'user_id',
            'catagory_id' => 'catagory_id',
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ];
    }
}
