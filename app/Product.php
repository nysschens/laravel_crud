<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'product_image', 'price'];

    public function getImageAttribute()
    {
        return $this->product_image;
    }
}
