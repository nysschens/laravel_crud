@extends('layouts.app')

@section('content')
  <div class="container" style="background-color: white;">

      <div class="clearfix">
          <br /><br />
          <div class="pull-left">
              <div class="lead" style="font-size: xx-large; font-family: 'Arial Black';">Produkte</div>
          </div>
          <br /><br /><br /><br /><br />
      </div>

      <table class="table table-bordered table-hover table-striped" style="border:none;">
          <tbody style="border: none;">
          @foreach($products as $product)
              <tr>
                  <td>
                      <a><img src="{{ asset('/images/'.$product->image) }}"></a>
                  </td>
                  <td>
                      <p class="card-header" style="font-size: xx-large">{{ $product->name }}<br /><br />
                      <p class="card-header" style="font-size: larger;">{{ $product->description }}

                      <div class="clearfix">
                          <br /><br />
                          <div class="input-group-btn" style="vertical-align: bottom; text-align: end;">
                              <!-- <a href="{-- route('product.edit', $product->id) }}" class="btn btn-primary">Edit</a> -->
                              <a href="{{ route('product.edit', $product->id) }}" class="btn btn-danger" style="border-top-right-radius: 0;border-bottom-right-radius: 0;">
                                  Mehr Erfahren
                              </a>
                          </div>
                      </div>
                  </td>

              </tr>
          @endforeach
          </tbody>
      </table>
  </div>

@endsection
