@extends('layouts.app')

@section('content')
  <div class="container" style="background-color: white;">

    <form method="POST" action="/product" enctype="multipart/form-data">

      <div class="clearfix">
          <br /><br /><br />
        <div class="pull-left">
          <div class="lead">
            <strong>Add New Produkt</strong>
          </div>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-success">Save</button>
          <a href="/product" class="btn btn-default">Back to list</a>
        </div>
      </div>
      <hr>

      @include('product.form')
    </form>

  </div>
@endsection
