{!! csrf_field() !!}

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">

  <label for="name" class="control-label">
    {{ trans('name') }}
  </label>

  <input type="text"
         name="name"
         id="name"
         value="{{ old('name', @$product->name) }}"
         placeholder="name"
         required
         class="form-control">

  @if ($errors->has('name'))
    <div class="help-block">
      {{ $errors->first('name') }}
    </div>
  @endif
</div>

<div class="form-group {{ $errors->has('product_image') ? 'has-error' : '' }}">
    <label for="product_image" class="control-label">
        {{ trans('product_image') }}
    </label>

    <input type="file"
           name="product_image"
           id="product_image"
           value="{{ old('product_image', @$product->image) }}"
           placeholder="product_image" class="form-control">

    @if ($errors->has('product_image'))
        <div class="help-block">
            {{ $errors->first('product_image') }}
        </div>
    @endif
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">

  <label for="price" class="control-label">
    {{ trans('price') }}
  </label>

  <input type="text"
         name="price"
         id="price"
         value="{{ old('price', @$product->price) }}"
         placeholder="price" class="form-control">

  @if ($errors->has('price'))
    <div class="help-block">
      {{ $errors->first('price') }}
    </div>
  @endif
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">

  <label for="content" class="control-label">
    {{ trans('description') }}
  </label>

      <textarea
          name="description"
          id="description"
          placeholder="description"
          required
          class="form-control">{{ old('description', @$product->description) }}</textarea>

  @if ($errors->has('description'))
    <div class="help-block">
      {{ $errors->first('description') }}
    </div>
  @endif
</div>

<div class="form-group">
  <button type="submit" class="btn btn-success">Save</button>
  <a href="/product" class="btn btn-default">Back to list</a>
</div>

