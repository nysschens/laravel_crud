@extends('layouts.app')

@section('content')

  <div class="container" style="background-color: white;">

    <h2>{{ $product->name }}</h2>

    <div>
      Posted by {{ $product->name }}
      <time class="timeago" datetime="{{ $product->updated_at->toIso8601String() }}"
            title="{{ $product->updated_at->toDayDateTimeString() }}">
        {{ $product->updated_at->diffForHumans() }}
      </time>
    </div>

    <hr>

    <div>
      {!! $product->descrition !!}
    </div>

  </div>

@endsection
