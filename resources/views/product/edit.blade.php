@extends('layouts.app')

@section('content')

    <div class="container" style="background-color: white;">

        <form method="POST" action="/product/{{ $product->id }}">
            <div class="clearfix">
              <br /><br /><br />
            <div class="pull-left">
              <div class="lead">
                <strong>Edit product</strong>
                <small>{{ $product->name }}</small>
              </div>
            </div>

            <div class="pull-right">
              <button type="submit" class="btn btn-success">Save</button>
              <a href="/product" class="btn btn-default">Back to list</a>
            </div>
          </div>
          <hr>

          {!! method_field('PUT') !!}
          @include('product.form')
        </form>

    </div>
@endsection
