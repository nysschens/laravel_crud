<!-- Footer -->
<footer class="footer text-left" style="margin-top: 40px;">
    <div class="container" style="background-color: white; width: 100%;">
        <div class="row" style="width: 100%;">

            <!-- Footer Location -->
            <div class="col-lg-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">&copy; Eg Crustag 2014</h4>
                <p class="lead mb-0" style="font-size: small;">Impressum
                    <br />
                </p>
            </div>

            <!-- Footer Social Icons -->
            <div class="col-lg-4 mb-5 mb-lg-0" style="margin-left: 180px;">
                <img src="{{ asset('/images/kontakt-footer.gif') }}">
            </div>

            <!-- Footer About Text -->
            <div class="col-lg-4">

            </div>

        </div>
    </div>
</footer>
